</div>
</div>
<!-- end content -->

<!-- begin footer -->
<div id="dokuwiki__footer">
    <div class="pad wrapper">
        <div class="cols">
            <div class="col logo"><img src="/images/logo-cesnet.svg" width="300" alt=""/></div>
            <div class="col">
                <h1 id="links" class="sectionedit1">Rychlé odkazy</h1>
                <div class="level1">
                    <ul>
                        <li class="level1">
                            <div class="li"><a href="https://www.eduroam.cz/" title="https://www.eduroam.cz/"
                                               rel="nofollow" class="urlextern">eduroam.cz</a></div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col">
                <h1 id="contact" class="sectionedit1">Kontakt</h1>
                <div class="level1">
                    <p>CESNET, z. s. p. o.<br/>Zikova 4, 16000 Prague 6<br/>Tel: +420 224 352 994<br/>Fax: +420 224 320
                        269<br/><a href="mailto:info@cesnet.cz" title="info@cesnet.cz" class="mail">info@cesnet.cz</a>
                    </p>
                </div>
            </div>
            <div class="col">
                <h1 id="service_desk" class="sectionedit1">Service desk</h1>
                <div class="level1">
                    <p>Tel: +420 224 352 994<br/>GSM: +420 602 252 531<br/>Fax: +420 224 313 211<br/><a
                                href="mailto:support@cesnet.cz" title="support@cesnet.cz"
                                class="mail">support@cesnet.cz</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="copyright">© 1996–<?= date('Y') ?> CESNET, z. s. p. o.</div>
    </div>
</div>
<!-- end footer -->

</div>
<script type="text/javascript" async src="https://linker2.cesnet.cz/linker.js"></script>
</body>
</html>
