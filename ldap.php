<?php
/**
 * Class LdapConnector - wrapper for communication with CESNET LDAP
 *
 * Author: Petr Vsetecka
 * Date: 7/26/18
 * Time: 11:23 AM
 */

class LdapConnector
{

    private $hostname;
    private $uid;
    private $passwd;
    private $filter;
    private $connection;
    private $answer;
    private $entries;

    /**
     * LdapConnector constructor.
     * @param $ini_array - config file to initialize the variables
     */
    public function __construct($ini_array)
    {
        $this->hostname = $ini_array["hostname"];
        $this->uid = $ini_array["uid"];
        $this->passwd = $ini_array["passwd"];
    }

    /**
     * Establishes a connection to the server
     */
    public function connect()
    {
        $this->connection = ldap_connect($this->hostname);

        // settings for connection with uid & password
        ldap_set_option($this->connection, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->connection, LDAP_OPT_REFERRALS, 0);

        if ($this->connection) {
            $bind = ldap_bind($this->connection, $this->uid, $this->passwd);
            return $bind;
        } else {
            return $this->connection;
        }
    }

    /**
     * @param $query - new filter enclosed in parentheses
     */
    public function setFilter($query)
    {
        $this->filter = $query;
    }

    /**
     * @param $baseDN
     */
    public function search($baseDN)
    {
        $this->answer = ldap_search($this->connection, $baseDN, $this->filter);
        $this->entries = ldap_get_entries($this->connection, $this->answer);
    }

    /**
     * disconnects properly from the server
     */
    public function closeConnection()
    {
        ldap_close($this->connection);
    }

    /**
     * @return int number of entries returned by the server
     */
    public function getEntryCount()
    {
        return ldap_count_entries($this->connection, $this->answer);
    }

    /**
     * @return mixed
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @return mixed
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * @return mixed
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @return mixed
     */
    public function getEntries()
    {
        return $this->entries;
    }
}
