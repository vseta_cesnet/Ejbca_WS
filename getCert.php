<?php
/**
 * Issue certificate with given parameters and offer download - step 2
 *
 * Author: Petr Vsetecka
 * Date: 8/16/18
 * Time: 11:01 AM
 */

include 'html_header.php';

$refresh = true;
$error = false;

if (!isset($_POST['user_email']) || !isset($_POST['user_server'])) {
    $error = true;
}

if (!isset($_SESSION['eduroam_admin'])) {
    if (($_POST['user_server'] < 0) || ($_POST['user_server'] > count($_SESSION['user_servers']))) {
        $error = true;
    }
}

if ($error) {
    print "<h5>$err_p3_0</h5>";
    include 'footer.php';
    exit;
}

/** variables **/
if (isset($_SESSION['eduroam_admin'])) {
    $server = $_POST['user_server'];
} else {
    $server = $_SESSION['user_servers'][$_POST['user_server']];
}
$email = $_POST['user_email'];
$dn = "CN=$server, O=CESNET, DC=cesnet-ca, DC=cz";
$ejbca_username = $server . "@eduroamCA";
$ejbca_userpass = $ini_array['ejbca_userpass'];

/** connect to ejbca **/
$ws_url = $ini_array["ws_url"];
$wsdl_url = $ini_array["wsdl_url"];
$client_cert = $ini_array["client_cert"];
$client_cert_password = $ini_array["client_cert_password"];
$ejbca = new Ejbca_Bridge($wsdl_url, array(), $ws_url, $client_cert, $client_cert_password);

try {
    if ($ejbca->userExists($ejbca_username)) {
        $user = $ejbca->getUserByUsername($ejbca_username);
    } else {
        $user = new EjbcaUser();
        $user->setUsername($ejbca_username);
        $user->setCaName("eduroam IdP SP CA");
        $user->setCertificateProfileName("eduroam hosted IdP");
        $user->setEndEntityProfileName("eduroam Hosted IdP EE");
        $user->setKeyRecoverable("");
        $user->setSendNotification("");
    }
    $user->setStatus(EjbcaUser::STATUS_NEW);
    $user->setEmail("");
    $user->setClearPwd("0");
    $user->setPassword($ejbca_userpass);
    $user->setSubjectDN($dn);
    $user->setSubjectAltName("RFC822NAME=" . $email . ", DNSNAME=" . $server);
    $user->setTokenType("P12");
    $ret = $ejbca->editUser($user);

    $cert = $ejbca->pkcs12Request($ejbca_username, $ejbca_userpass, "0", "2048", "RSA");

    /** transform the returned keystore to format users understand **/
    $old_path = getcwd();
    $sid = session_id();
    $filename = "/tmp/$sid-certs/certs.zip";
    $output = shell_exec("mkdir /tmp/$sid-certs");
    chdir("/tmp/$sid-certs");
    $output = shell_exec("rm -rf ./*");
    $hostname_folder = str_replace('.', '_', $server);
    $output = shell_exec("mkdir $hostname_folder");
    $output = shell_exec("cp /etc/ssl/certs/chain_CESNET_CA4.pem $hostname_folder/.");
    $output = shell_exec("cp /var/www/eduroam/readme.txt $hostname_folder/.");
    $output = shell_exec("cp /var/www/eduroam/readme.html $hostname_folder/.");
    file_put_contents("keystore-enc", $cert);
    $output = shell_exec("base64 -d keystore-enc > keystore");
    $output = shell_exec("openssl pkcs12 -in keystore -out certs.pem -nodes -passin pass:$ejbca_userpass");
    $output = shell_exec("openssl pkey -in certs.pem -out $hostname_folder/radius.key");
    $output = shell_exec("openssl x509 -in certs.pem -outform PEM -out $hostname_folder/radius.crt");
    $output = shell_exec("openssl crl2pkcs7 -nocrl -certfile certs.pem |
        openssl pkcs7 -print_certs -out cert-chain.pem");
    $output = shell_exec("awk -v RS= '{print > (\"cert-\" NR \".pem\")}' cert-chain.pem");
    $output = shell_exec("cp cert-6.pem $hostname_folder/eduroamCA.pem");
    $output = shell_exec("openssl pkcs12 -export -in certs.pem -out $hostname_folder/radius.p12 -passout pass:");
    $output = shell_exec("zip -r certs.zip $hostname_folder");
    chdir($old_path);

    /** save variables necessary for download **/
    $_SESSION['file'] = $filename;
    $_SESSION['zip_name'] = $hostname_folder . ".zip";

    /** offer download **/
    $p3_0 = str_replace('$server', $server, $p3_0);
    print "        <p>$p3_0</p>" . PHP_EOL;

} catch (SoapFault $e) {
    error_log("[EJBCA WS ERROR] " . $e->getMessage() . "\n");
    print "        <h5>$err_p2_0</h5>";
} catch (Throwable $e) {
    error_log("[PHP ERROR] " . $e->getMessage() . "\n");
    print "        <h5>$err_p2_0</h5>";
}

/** add footer **/
include 'footer.php';

