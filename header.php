<?php
/**
 * Common header for all pages
 * includes SimpleSAML library and timer
 *
 * Author: Petr Vsetecka
 * Date: 8/16/18
 * Time: 4:14 PM
 */

$ini_array = parse_ini_file("/etc/fedra/eduroam.ini");
$language = $ini_array["language"];
require_once "$language";

require_once '/var/simplesamlphp-eduroam/lib/_autoload.php';
require_once 'ejbcaws.php';
require_once 'ldap.php';

/***** Authenticate user *****/
$auth = new \SimpleSAML\Auth\Simple('default-sp');
$auth->requireAuth(array(
    'ReturnTo' => 'https://certifikat.eduroam.cz/',
    'KeepPost' => FALSE,
));

if(!isset($_SESSION)) {
    session_start();
}

