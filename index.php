<?php
/**
 * Get parameters for certificate from authenticated user - step 1
 *
 * Authors: Petr Vsetecka, Jan Chvojka
 * Date: 12/9/19
 * Time: 4:00 PM
 */

include 'html_header.php';

/** load variables **/
$ldap_people_baseDN = $ini_array["people_baseDN"];
$ldap_srv_baseDN = $ini_array["srv_baseDN"];
$proxy_personID = 'urn:cesnet:proxyidp:attribute:forwardedPersonPrincipalName';

/** connect to the LDAP server **/
$ldap = new LdapConnector($ini_array);
$ldap_connection = $ldap->connect();

if (!$ldap_connection) {
    error_log("[LDAP ERROR] couldn't connect to server\n");
    print "<h4>$err_p2_0</h4>";
    include 'footer.php';
    exit;
}

/** get user name from federation **/
$attributes = $auth->getAttributes();
if (!isset($attributes["$proxy_personID"])) {
    error_log("[SimpleSAML ERROR] $proxy_personID attribute missing\n");
    print "<h4>$err_p2_1</h4>";
    include 'footer.php';
    exit;
}

/** search for user records **/
$ldap_filter = "(|"; 
$search_attribute = $attributes["$proxy_personID"];
foreach ($search_attribute as $key => $value) {
    $ldap_filter .= "(eduPersonPrincipalNames=$value)"; 
}
$ldap_filter .= ")"; 
$ldap->setFilter($ldap_filter);
$ldap->search($ldap_people_baseDN);
$ldap_answer = $ldap->getEntries();

if ($ldap_answer["count"] == 0) {
    error_log("[LDAP WARNING] no user accounts found in LDAP\n");
    print "<h4>";
    echo str_replace("<search_attribute>", "</h4>".implode("<br>", $search_attribute)."<h4>", $err_p2_2a);
    print "</h4>";
    include 'footer.php';
    exit;
}
if ($ldap_answer["count"] > 1) {
    error_log("[LDAP WARNING] too many user accounts found in LDAP\n");
    print "<h4>";
    echo str_replace("<search_attribute>", "</h4>".implode("<br>", $search_attribute)."<h4>", $err_p2_2b);
    print "</h4>";
    include 'footer.php';
    exit;
}

/** start printing the page **/
print "        <p>$p2_0</p>" . PHP_EOL;
print "        <h4>$p2_1";
if (isset($attributes['displayName'][0])) {
    print $attributes["displayName"][0];
} else if (isset($attributes['cn'][0])) {
    trigger_error("Attribute 'displayName' is missing, using 'cn' instead.", E_USER_NOTICE);
    print $attributes["cn"][0];
} else {
    trigger_error("Attributes 'displayName' and 'cn' are missing, fallback to LDAP.", E_USER_WARNING);
    print $ldap_answer[0]["cn"][0];
}
print "</h4>" . PHP_EOL . PHP_EOL;

/** email selection **/
print '        <form action="getCert.php" method="POST">' . PHP_EOL;
print "            <p>$p2_2</p>" . PHP_EOL;
$user_email = $ldap_answer[0]["mail"][0];
print "            <input type=\"email\" name=\"user_email\" value=\"$user_email\" pattern=\"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,3}$\" title=\"$form_mail\">" . PHP_EOL . "<br><br>" . PHP_EOL;


/** check if user is admin **/
unset($_SESSION['eduroam_admin']);
if (isset($attributes['eduPersonEntitlement'][0])) {
    foreach ($attributes['eduPersonEntitlement'] as $entitlement) {
        if ($entitlement == "urn:geant:cesnet.cz:group:einfra:eduroamManagers#perun.cesnet.cz") {
            $_SESSION['eduroam_admin'] = true;
            break;
        }
    }
}

if ($_SESSION['eduroam_admin']) {
    print "        <p>$p2_3</p>" . PHP_EOL;
    print '            <input id="user_server" type="text" name="user_server" required>' . PHP_EOL;
} else {
    /** search for managed servers **/
    $ldap_filter = $ini_array["srv_filter"];
    $ldap_filter = str_replace('$DN', $ldap_answer[0]["dn"], $ldap_filter);
    $ldap->setFilter($ldap_filter);
    $ldap->search($ldap_srv_baseDN);
    $ldap_answer = $ldap->getEntries();

    if ($ldap_answer["count"] == 0) {
        print $err_p2_3;
        include 'footer.php';
        exit;
    }

    /** print user's managed servers **/
    print "        <p>$p2_4</p>" . PHP_EOL;
    $user_servers = array();
    for ($i = 0; $i < $ldap_answer["count"]; $i++) {
        $server = $ldap_answer[$i]["cn"][0];
        $user_servers[] = $server;
        print "            <div><input type=\"radio\" name=\"user_server\" id=\"$i\" value=\"$i\" required>" . PHP_EOL;
        print "                <label for=\"$i\">$server</label></div>" . PHP_EOL;
    }
    $_SESSION['user_servers'] = $user_servers;
}

$ldap->closeConnection();

/** close the page **/
print "            <br>
            <br>
            <input type=\"submit\" name=\"submit\" value=\"$button_issue\">" . PHP_EOL;
print '        </form>' . PHP_EOL;
include 'footer.php';
