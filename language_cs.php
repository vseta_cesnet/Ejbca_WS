<?php
/**
 * Czech texts
 *
 * Author: Petr Vsetecka
 * Date: 8/21/18
 * Time: 2:56 PM
 */

$lang = "cs";
$header = "eduroam CA";
$title = "eduroam CA";

$p2_0 = "<a href=\"https://www.eduroam.cz/cs/spravce/pripojovani/eduroamca\">Certifikační autorita</a> sloužící pro automatizované vydávání certifikátů pro RadSec / IPsec spojení s národním RADIUSem.";
$p2_1 = "Jste přihlášen jako ";
$p2_2 = "Zadejte email, kam bude zasláno upozornění na blížící se expiraci certifikátu:";
$p2_3 = "Zadejte server, ke kterému chcete vydat certifikát:";
$p2_4 = "Vyberte server, ke kterému chcete vydat certifikát:";
$form_mail = "Emailová adresa";

$p3_0 = 'Certifikát pro $server byl vygenerován. Pro jeho stažení klikněte <a href="download.php" target="_blank">zde</a>.';

$button_issue = "Vydat certifikát";
$button_download = "Stáhnout certifikát";

$link_login = "Login";
$link_logout = "Logout";

$err_EJBCA = 'Missing EJBCA library!';
$err_SAML = 'Missing SAML library!';
$err_LDAP = 'Missing LDAP library!';

$err_p2_0 = 'Nedaří se nám spojit se vzdáleným serverem. Zkuste to prosím později.';
$err_p2_1 = 'Chyba při získávání dat. Zkuste to prosím znovu.';
$err_p2_2a = 'Nenalezen záznam, že by některý z účtů: <search_attribute> byl evidován jako eduroam admin. Pokud problém přetrvává, kontaktujte <a href="mailto:info@eduroam.cz">info@eduroam.cz</a>.';
$err_p2_2b = 'Nalezen víc než jeden záznam eduroam admina pro účty: <search_attribute>. Pokud problém přetrvává, kontaktujte <a href="mailto:info@eduroam.cz">info@eduroam.cz</a>.';
$err_p2_3 = 'Nemáte ve své správě žádné servery. Pokud myslíte, že je to chyba, kontaktujte <a href="mailto:info@eduroam.cz">info@eduroam.cz</a>.';

$err_p3_0 = 'Došlo k chybě při načítání formuláře.';
