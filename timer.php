<?php
/**
 * Common footer for all pages
 * prints out the timer
 *
 * Author: Petr Vsetecka
 * Date: 8/16/18
 * Time: 4:32 PM
 */

$end_time = microtime(TRUE);
$time_taken = $end_time - $start_time;
$time_taken = round($time_taken, 3);

print PHP_EOL . '<!-- Page generated in ' . $time_taken . ' seconds. -->' . PHP_EOL;
