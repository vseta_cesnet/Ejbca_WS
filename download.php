<?php
/**
 * Download page - step 3
 *
 * Author: Petr Vsetecka
 * Date: 8/16/18
 * Time: 12:59 PM
 */

include 'header.php';

/** @var $auth - needed for valid SESSION */
$auth = new SimpleSAML_Auth_Simple('default-sp');
$auth->requireAuth();

if (!isset($_SESSION['file']) || !isset($_SESSION['zip_name']))
    exit;

$file = $_SESSION['file'];
$zip_name = $_SESSION['zip_name'];

// mind: do sanity checks on the "file" variable to prevent people from stealing files such as don't accept file extensions, deny slashes, add .pdf to the value

/** old IE (6 & 7) headers **/
//header("Pragma: public");
//header("Expires: -1");
//header("Cache-Control: public, must-revalidate, post-check=0, pre-check=0");

header("Content-Type: application/zip");
header("Content-Disposition: attachment; filename=$zip_name");
header("Content-Length: " . filesize($file));
readfile($file);
exit;
