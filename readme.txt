Certifikační autorita eduroam CA slouží k vydávání certifikátů pro RADIUS servery které mají být připojeny pomocí RadSec anebo IPsec k české eduroam infrastruktuře. Nedoporučujeme použití pro EAP, tam použijte CA kterou vaši uživatelé znají.

Pokud používáte Linux, budete potřebovat soubory radius.crt a radius.key, které obsahují certifikát resp. privátní klíč pro váš RADIUS server. V souboru eduroamCA.pem je certifikát certifikační autority eduroam CA, pravděpodobně ho nebudete potřebovat. 

Pokud používáte OS Windows, bude se vám víc hodit soubor radius.p12, který obsahuje certifikát, privátní klíč a certifikát podpisující CA. Soubor je nešifrovaný, pokud budete dotázáni na heslo, stačí stisknout Enter.

Informace o eduroam CA:
    https://www.eduroam.cz/cs/spravce/pripojovani/eduroamca

Základní práce s certifikáty:
    https://www.eduroam.cz/cs/spravce/pripojovani/serverove_certifikaty

Národní RADIUS NEpoužívá eduroam CA ale CESNET CA4:
    https://pki.cesnet.cz/cs/ch-cca-crt-crl.html

Postup připojení k eduroamu v ČR:
    https://www.eduroam.cz/cs/spravce/pripojovani/uvod

Kontakt:
    info@eduroam.cz

