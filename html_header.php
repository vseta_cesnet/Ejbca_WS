<?php
/**
 * Common HTML header including the linker, title & stylesheet
 *
 * Author: Petr Vsetecka
 * Date: 29.8.18
 * Time: 10:48
 */

include 'header.php';

print "<!DOCTYPE html>
<html lang=\"$lang\">
<head>
    <meta charset=\"UTF-8\">" . PHP_EOL;

if (isset($refresh) && $refresh && ($error == false))
    print '   <meta http-equiv="refresh" content="1;url=download.php">' . PHP_EOL;

print "   <title>$header</title>
    <link rel=\"stylesheet\" href=\"style.css\">
    <link rel=\"shortcut icon\" href=\"images/favicon.ico\">
</head>
<body>
<div id=\"container\">
    <div id=\"cesnet_linker_placeholder\"></div>
    
    <!-- begin content -->
    <div class=\"main\">
      <div class=\"main_text\">
        <h1 id=\"title\">$title</h1>" . PHP_EOL;
