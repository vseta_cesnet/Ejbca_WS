<?php

// requires php module soap installed & enabled

//require_once './IniParser.php';

class Certificate {
    public $issuer;
    public $subject;
    public $validFrom;
    public $validFromStr;
    public $validTo;
    public $validToStr;
    public $serial;
    public $revoked;
    public $revocationDate;
    public $certificateData;
    public $certificateParsedData;

    function __construct($pemData) {
        $o = "";
        $acert = "-----BEGIN CERTIFICATE-----\n" . $pemData . "\n-----END CERTIFICATE-----\n";
        exec("echo \"$acert\" | openssl x509 -noout -text -nameopt oneline,-esc_msb", $o);
        $s = implode("\n", $o);

        $this->certificateData = $acert;
        $this->certificateParsedData = $s;
        
        // subject
        $sub1 = strpos($s, "Subject: ");
        if($sub1===false) {
            $this->subject = "[none]";
        }
        else {
            $sub2 = strpos($s, "\n", $sub1+10);
            $this->subject = substr($s, $sub1+9, $sub2-$sub1-9);
        }

        // issuer
        $sub1 = strpos($s, "Issuer: ");
        if($sub1===false) {
            $this->issuer = "[none]";
        }
        else {
            $sub2 = strpos($s, "\n", $sub1+9);
            $this->issuer = substr($s, $sub1+8, $sub2-$sub1-8);
        }

        // serial
        $sub1 = strpos($s, "Serial Number:");
        if($sub1===false) {
            $this->serial = "[none]";
        }
        else {
            $sub2 = strpos($s, "(", $sub1+1);
            $sr = str_replace(":", "", trim(substr($s, $sub1+14, $sub2-$sub1-14)));
            $this->serial = dechex($sr);
        }
        
        // platnost od
        $sub1 = strpos($s, "Not Before: ");
        if($sub1===false) {
            $this->validFrom = "[none]";
        }
        else {
            $sub2 = strpos($s, "\n", $sub1+10);
            $sr = trim(substr($s, $sub1+12, $sub2-$sub1-10));
            $this->validFrom = DateTime::createFromFormat("M j G:i:s Y T", $sr);
            $this->validFromStr = $this->validFrom->format("j. n. Y G:i:s");
        }
        
        // platnost do
        $sub1 = strpos($s, "Not After : ");
        if($sub1===false) {
            $this->validTo = "[none]";
        }
        else {
            $sub2 = strpos($s, "\n", $sub1+10);
            $sr = trim(substr($s, $sub1+12, $sub2-$sub1-10));
            $this->validTo = DateTime::createFromFormat("M j G:i:s Y T", $sr);
            $this->validToStr = $this->validTo->format("j. n. Y G:i:s");
        }
    }
}

class EjbcaUser {

    const STATUS_FAILED      =  11;
    const STATUS_GENERATED   =  40;
    const STATUS_HISTORICAL  =  60;
    const STATUS_INITIALIZED =  20;
    const STATUS_INPROCESS   =  30;
    const STATUS_KEYRECOVERY =  70;
    const STATUS_NEW         =  10;
    const STATUS_REVOKED     =  50;
    
    private $caName;
    private $certificateProfileName;
    private $password;
    private $clearPwd;
    private $email;
    private $endEntityProfileName;
    private $keyRecoverable;
    private $sendNotification;
    private $status;
    private $subjectAltName;
    private $subjectDN;
    private $tokenType;
    private $username;

    public function setCaName($caName) {
        $this->caName = $caName;
    }
    
    public function getCaName() {
        return $this->caName;
    }

    public function setCertificateProfileName($certificateProfileName) {
        $this->certificateProfileName = $certificateProfileName;
    }

    public function getCertificateProfileName() {
        return $this->certificateProfileName;
    }

    public function setClearPwd($clearPwd) {
        $this->clearPwd = $clearPwd;
    }
    
    public function getClearPwd() {
        return $this->clearPwd;
    }
    
    public function setPassword($password) {
        $this->password = $password;
    }
    
    public function getPassword() {
        return $this->password;
    }

    public function setEmail($email) {
        $this->email = $email;
    }
    
    public function getEmail() {
        return $this->email;
    }
    
    public function setEndEntityProfileName($endEntityProfileName) {
        $this->endEntityProfileName = $endEntityProfileName;
    }
    
    public function getEndEntityProfileName() {
        return $this->endEntityProfileName;
    }
    
    public function setKeyRecoverable($keyRecoverable) {
        $this->keyRecoverable = $keyRecoverable;
    }
    
    public function getKeyRecoverable() {
        return $this->keyRecoverable;
    }
    
    public function setSendNotification($sendNotification) {
        $this->sendNotification = $sendNotification;
    }
    
    public function getSendNotification() {
        return $this->sendNotification;
    }

    public function setStatus($status) {
        $this->status = $status;
    }
    
    public function getStatus() {
        return $this->status;
    }
    
    public function setSubjectAltName($subjectAltName) {
        $this->subjectAltName = $subjectAltName;
    }
    
    public function getSubjectAltName() {
        return $this->subjectAltName;
    }
    
    public function setSubjectDN($subjectDN) {
        $this->subjectDN = $subjectDN;
    }
    
    public function getSubjectDN() {
        return $this->subjectDN;
    }
    
    public function setTokenType($tokenType) {
        $this->tokenType = $tokenType;
    }
    
    public function getTokenType() {
        return $this->tokenType;
    }
    
    public function setUsername($username) {
        $this->username = $username;
    }
    
    public function getUsername() {
        return $this->username;
    }
    
}

class Ejbca_SoapClient extends SoapClient { 
    
    private $ws_url;
    private $client_cert;
    private $client_cert_password;
    private $ws_head = array('Content-Type: text/xml', 'SOAPAction: ""');
    
    public function __construct(
            $wsdl,
            $options = array(),
            $ws_url,
            $client_cert,
            $client_cert_password) {
        parent::__construct($wsdl, $options);
        $this->ws_url = $ws_url;
        $this->client_cert = $client_cert;
        $this->client_cert_password = $client_cert_password;
    }     
    
    protected function callCurl($url, $data) { 
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $this->ws_url);
        curl_setopt($handle, CURLOPT_FAILONERROR, false);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSLCERT, $this->client_cert);
        curl_setopt($handle, CURLOPT_SSLCERTPASSWD, $this->client_cert_password);
        curl_setopt($handle, CURLOPT_HTTPHEADER, $this->ws_head);
        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($handle, CURLOPT_SSL_CIPHER_LIST, "AES128-SHA AES256-SHA DH-DSS-AES128-SHA DH-DSS-AES256-SHA DH-RSA-AES128-SHA DH-RSA-AES256-SHA DHE-DSS-AES128-SHA DHE-DSS-AES256-SHA DHE-RSA-AES128-SHA DHE-RSA-AES256-SHA ADH-AES128-SHA ADH-AES256-SHA");
        $response = curl_exec($handle);
        if (empty($response)) {
            throw new SoapFault('CURL error: '.curl_error($handle),curl_errno($handle));
        }
        curl_close($handle);
        return $response;
    }    
        
    public function __doRequest($request, $location, $action, $version, $one_way = 0) {
#	echo("doRequest\n");
        return $this->callCurl($location, $request);
    } 
    
}

class Ejbca_Bridge {
    
    private $soapClient;
    
    const MATCH_TYPE_BEGINSWITH         =   1;
    const MATCH_TYPE_CONTAINS           =   2;
    const MATCH_TYPE_EQUALS             =   0;
    const MATCH_WITH_CA                 =   5;
    const MATCH_WITH_CERTIFICATEPROFILE =   4;
    const MATCH_WITH_COMMONNAME         = 101;
    const MATCH_WITH_COUNTRY            = 112;
    const MATCH_WITH_DIRECTORYNAME      = 204;
    const MATCH_WITH_DN                 =   7;
    const MATCH_WITH_DNSERIALNUMBER     = 102;
    const MATCH_WITH_DNSNAME            = 201;
    const MATCH_WITH_DOMAINCOMPONENT    = 111;
    const MATCH_WITH_EDIPARTNAME        = 205;
    const MATCH_WITH_EMAIL              =   1;
    const MATCH_WITH_ENDENTITYPROFILE   =   3;
    const MATCH_WITH_GIVENNAME          = 103;
    const MATCH_WITH_GUID               = 209;
    const MATCH_WITH_INITIALS           = 104;
    const MATCH_WITH_IPADDRESS          = 202;
    const MATCH_WITH_LOCALE             = 109;
    const MATCH_WITH_ORGANIZATION       = 108;
    const MATCH_WITH_ORGANIZATIONUNIT   = 107;
    const MATCH_WITH_REGISTEREDID       = 207;
    const MATCH_WITH_RFC822NAME         = 200;
    const MATCH_WITH_STATE              = 110;
    const MATCH_WITH_STATUS             =   2;
    const MATCH_WITH_SURNAME            = 105;
    const MATCH_WITH_TITLE              = 106;
    const MATCH_WITH_TOKEN              =   6;
    const MATCH_WITH_UID                = 100;
    const MATCH_WITH_UPN                = 208;
    const MATCH_WITH_URI                = 206;
    const MATCH_WITH_USERNAME           =   0;
    const MATCH_WITH_X400ADDRESS        = 203;

    const TOKEN_TYPE_JKS                = "JKS";
    const TOKEN_TYPE_P12                = "P12";
    const TOKEN_TYPE_PEM                = "PEM";
    const TOKEN_TYPE_USERGENERATED      = "USERGENERATED";
    
    const VIEW_RIGHTS                                = "/view_end_entity";
    const EDIT_RIGHTS                                = "/edit_end_entity";
    const CREATE_RIGHTS                              = "/create_end_entity";
    const DELETE_RIGHTS                              = "/delete_end_entity";
    const REVOKE_RIGHTS                              = "/revoke_end_entity";
    const HISTORY_RIGHTS                             = "/view_end_entity_history";
    const APPROVAL_RIGHTS                            = "/approve_end_entity";
    const HARDTOKEN_RIGHTS                           = "/view_hardtoken";
    const HARDTOKEN_PUKDATA_RIGHTS                   = "/view_hardtoken/puk_data";
    const KEYRECOVERY_RIGHTS                         = "/keyrecovery";

    const ENDENTITYPROFILEBASE                       = "/endentityprofilesrules";
    const ENDENTITYPROFILEPREFIX                     = "/endentityprofilesrules/";
    const USERDATASOURCEBASE                         = "/userdatasourcesrules";
    const USERDATASOURCEPREFIX                       = "/userdatasourcesrules/";
    const UDS_FETCH_RIGHTS                           = "/fetch_userdata";
    const UDS_REMOVE_RIGHTS                          = "/remove_userdata";

    const CABASE                                     = "/ca";
    const CAPREFIX                                   = "/ca/";
    const ROLE_PUBLICWEBUSER                         = "/public_web_user";
    const ROLE_ADMINISTRATOR                         = "/administrator";
    const ROLE_SUPERADMINISTRATOR                    = "/super_administrator";
    const REGULAR_CAFUNCTIONALTY                     = "/ca_functionality";
    const REGULAR_CABASICFUNCTIONS                   = "/ca_functionality/basic_functions";
    const REGULAR_ACTIVATECA                         = "/ca_functionality/basic_functions/activate_ca";
    const REGULAR_RENEWCA                            = "/ca_functionality/renew_ca";
    const REGULAR_VIEWCERTIFICATE                    = "/ca_functionality/view_certificate";
    const REGULAR_APPROVECAACTION                    = "/ca_functionality/approve_caaction";
    const REGULAR_CREATECRL                          = "/ca_functionality/create_crl";
    const REGULAR_EDITCERTIFICATEPROFILES            = "/ca_functionality/edit_certificate_profiles";
    const REGULAR_CREATECERTIFICATE                  = "/ca_functionality/create_certificate";
    const REGULAR_STORECERTIFICATE                   = "/ca_functionality/store_certificate";
    const REGULAR_RAFUNCTIONALITY                    = "/ra_functionality";
    const REGULAR_EDITENDENTITYPROFILES              = "/ra_functionality/edit_end_entity_profiles";
    const REGULAR_EDITUSERDATASOURCES                = "/ra_functionality/edit_user_data_sources";
    const REGULAR_VIEWENDENTITY                      = "/ra_functionality/view_end_entity";
    const REGULAR_CREATEENDENTITY                    = "/ra_functionality/create_end_entity";
    const REGULAR_EDITENDENTITY                      = "/ra_functionality/edit_end_entity";
    const REGULAR_DELETEENDENTITY                    = "/ra_functionality/delete_end_entity";
    const REGULAR_REVOKEENDENTITY                    = "/ra_functionality/revoke_end_entity";
    const REGULAR_VIEWENDENTITYHISTORY               = "/ra_functionality/view_end_entity_history";
    const REGULAR_APPROVEENDENTITY                   = "/ra_functionality/approve_end_entity";
    const REGULAR_LOGFUNCTIONALITY                   = "/log_functionality";
    const REGULAR_VIEWLOG                            = "/log_functionality/view_log";
    const REGULAR_LOGCONFIGURATION                   = "/log_functionality/edit_log_configuration";
    const REGULAR_LOG_CUSTOM_EVENTS                  = "/log_functionality/log_custom_events";
    const REGULAR_SYSTEMFUNCTIONALITY                = "/system_functionality";
    const REGULAR_EDITADMINISTRATORPRIVILEDGES       = "/system_functionality/edit_administrator_privileges";
    const REGULAR_EDITSYSTEMCONFIGURATION            = "/system_functionality/edit_systemconfiguration";
    const REGULAR_VIEWHARDTOKENS                     = "/ra_functionality/view_hardtoken";
    const REGULAR_VIEWPUKS                           = "/ra_functionality/view_hardtoken/puk_data";
    const REGULAR_KEYRECOVERY                        = "/ra_functionality/keyrecovery";

    const HARDTOKEN_HARDTOKENFUNCTIONALITY           = "/hardtoken_functionality";
    const HARDTOKEN_EDITHARDTOKENISSUERS             = "/hardtoken_functionality/edit_hardtoken_issuers";
    const HARDTOKEN_EDITHARDTOKENPROFILES            = "/hardtoken_functionality/edit_hardtoken_profiles";
    const HARDTOKEN_ISSUEHARDTOKENS                  = "/hardtoken_functionality/issue_hardtokens";
    const HARDTOKEN_ISSUEHARDTOKENADMINISTRATORS     = "/hardtoken_functionality/issue_hardtoken_administrators";

    const RESPONSETYPE_CERTIFICATE                   = "CERTIFICATE";
    const RESPONSETYPE_PKCS7                         = "PKCS7";
    const RESPONSETYPE_PKCS7WITHCHAIN                = "PKCS7WITHCHAIN";
    
    const NOT_REVOKED                            = -1;
    const REVOKATION_REASON_UNSPECIFIED          =  0;
    const REVOKATION_REASON_KEYCOMPROMISE        =  1;
    const REVOKATION_REASON_CACOMPROMISE         =  2;
    const REVOKATION_REASON_AFFILIATIONCHANGED   =  3;
    const REVOKATION_REASON_SUPERSEDED           =  4;
    const REVOKATION_REASON_CESSATIONOFOPERATION =  5;
    const REVOKATION_REASON_CERTIFICATEHOLD      =  6;
    const REVOKATION_REASON_REMOVEFROMCRL        =  8;
    const REVOKATION_REASON_PRIVILEGESWITHDRAWN  =  9;
    const REVOKATION_REASON_AACOMPROMISE         = 10;

    public function __construct($wsdl, $options = array(), $ws_url, $client_cert, $client_cert_password) {
        $this->soapClient = new Ejbca_SoapClient($wsdl, $options, $ws_url, $client_cert, $client_cert_password);
    }

    /**
     *
     * @param type $loginName
     * @param type $validOnly
     * @return type 
     */
    public function findCerts($loginName, $validOnly) {
	try {
        $ret = $this->soapClient->__soapCall("findCerts", array(array(
            "arg0"=>$loginName, 
            "arg1"=>$validOnly)));
	}
	catch (Exception $e) {
	    $a = print_r($e, true);
	    
	}

        $a = print_r($ret, true);
        $certs = array();
        $c = $ret->return;
	if(is_array($c)) {
            foreach($c as $cert) {
	        $tmpCert = new Certificate($cert->certificateData);
    	        $certs[] = $tmpCert;
    	    }
	    return $certs;
	}
	else {
#	    echo("Jeden cert.");
	    $cert = new Certificate($c->certificateData);
	    return $cert;
	}
    }

    /**
     *
     * @return type EjbCA version
     */
    public function getEjbcaVersion() {
        $ret = $this->soapClient->__soapCall("getEjbcaVersion", array());
        return $ret;
    }

    /**
     *
     * @return type List of available web services functions
     */
    public function getFunctions() {
        return $this->soapClient->__getFunctions();
    }

    public function getCertificate($certSNinHex, $issuerDN) {
        $ret = $this->soapClient->__soapCall("getCertificate", array(array(
            "arg0"=>$certSNinHex,
            "arg1"=>$issuerDN)));
        return $ret;
    }

    /**
     * nezajem
     *
     * @param $admin
     * @param $resource
     * @return mixed
     */
    public function isAuthorized($admin, $resource) {
        $ret = $this->soapClient->__soapCall("isAuthorized", array(array(
            "arg0"=>$admin, 
            "arg1"=>$resource)));
        return $ret;
    }

    /**
     * nezajem
     *
     * @param isApproved $parameters
     * @return mixed
     */
    public function isApproved(isApproved $parameters) {
        $ret = $this->soapClient->__soapCall("isApproved", array());
        return $ret;
    }

    /**
     * nezajem
     *
     * @param $hardTokenSN
     * @return mixed
     */
    public function existsHardToken($hardTokenSN) {
        $ret = $this->soapClient->__soapCall("existsHardToken", array(array(
            "arg0"=>$hardTokenSN)));
        return $ret;
    }

    /**
     *
     * @return type List of available CA's
     */
    public function getAvailableCAs() {
        $ret = $this->soapClient->__soapCall("getAvailableCAs", array());
        return $ret;
    }
    
    public function revokeUser($username, $reason, $deleteUser) {
        $ret = $this->soapClient->__soapCall("revokeUser", array(array(
            "arg0"=>$username, 
            "arg1"=>$reason, 
            "arg2"=>$deleteUser)));
        return $ret;
    }
    
    public function revokeCert($issuerDN, $certificateSN, $reason) {
        $ret = $this->soapClient->__soapCall("revokeCert", array(array(
            "arg0"=>$issuerDN, 
            "arg1"=>$certificateSN,
	    "arg2" => "chvojka@vpn.cesnet.cz", 
            "arg3"=>$reason)));
        return $ret;
    }
    
    public function findUser($matchwith, $matchtype, $matchvalue) {
        return $this->soapClient->__soapCall("findUser", array(array(
            "arg0"=>array(
                "matchtype" => $matchtype, 
                "matchvalue"=> $matchvalue, 
                "matchwith" => $matchwith))));
    }
    
    /**
     *
     * @param string $userName
     * @return bool
     */
    public function userExists($userName) {
        $ret = $this->soapClient->__soapCall("findUser", array(array(
            "arg0"=>array(
                "matchtype" => Ejbca_Bridge::MATCH_TYPE_EQUALS, 
                "matchvalue"=> $userName, 
                "matchwith" => Ejbca_Bridge::MATCH_WITH_USERNAME))));
        if(isset($ret->return)) {
            return true;
        }
        else {
            return false;
        }
    }

    public function createCRL($caname) {
        $ret = $this->soapClient->__soapCall("createCRL", array(array(
            "arg0"=>$caname)));
        return $ret;
    }
    
    /**
     *
     * @param string $userName
     * @return EjbcaUser 
     */
    public function getUserByUsername($userName) {
        $ret = $this->soapClient->__soapCall("findUser", array(array(
            "arg0"=>array(
                "matchtype" => Ejbca_Bridge::MATCH_TYPE_EQUALS, 
                "matchvalue"=> $userName, 
                "matchwith" => Ejbca_Bridge::MATCH_WITH_USERNAME))));
        if(isset($ret->return)) {
            $user = new EjbcaUser();
            $user->setCaName($ret->return->caName);
            $user->setCertificateProfileName($ret->return->certificateProfileName);
            $user->setClearPwd($ret->return->clearPwd);
            $user->setEmail($ret->return->email);
            $user->setEndEntityProfileName($ret->return->endEntityProfileName);
            $user->setKeyRecoverable($ret->return->keyRecoverable);
            $user->setsendNotification($ret->return->sendNotification);
            $user->setStatus($ret->return->status);
            $user->setSubjectAltName($ret->return->subjectAltName);
            $user->setTokenType($ret->return->tokenType);
            $user->setUsername($ret->return->username);
            return $user;
        }
        else {
            return false;
        }
    }
    
    /**
     *
     * @param EjbcaUser $user
     * @return EjbcaUser
     */
    public function editUser($user) {
        $ret = $this->soapClient->__soapCall("editUser", array(array(
            "arg0" => array(
                "caName"=>$user->getCaName(), 
                "certificateProfileName"=>$user->getCertificateProfileName(), 
                "clearPwd"=>$user->getClearPwd(),
                "email"=>$user->getEmail(),
                "endEntityProfileName"=>$user->getEndEntityProfileName(),
                "keyRecoverable"=>$user->getKeyRecoverable(),
                "sendNotification"=>$user->getSendNotification(),
                "status"=>$user->getStatus(),
                "subjectAltName"=>$user->getSubjectAltName(),
                "subjectDN"=>$user->getSubjectDN(),
                "tokenType"=>$user->getTokenType(),
                "username"=>$user->getUsername(),
                "password"=>"abcd",
                )
        )));
        return $ret;
    }
    
    public function getLastCertChain($caname) {
        $ret = $this->soapClient->__soapCall("getLastCertChain", array(array(
            $caname
        )));
        return $ret;
    }
    
    public function crmfRequest(crmfRequest $parameters) {
        $ret = $this->soapClient->__soapCall("crmfRequest", array());
        return $ret;
    }

    /**
     *
     * @param type $username
     * @param type $password
     * @param type $spkac
     * @param type $hardTokenSN
     * @param type $responseType
     * @return type 
     */
    public function spkacRequest(
            $username, 
            $password, 
            $spkac, 
            $hardTokenSN, 
            $responseType) {
        $ret = $this->soapClient->__soapCall("spkacRequest", array(array(
            "arg0"=>$username, 
            "arg1"=>$password, 
            "arg2"=>$spkac, 
            "arg3"=>$hardTokenSN, 
            "arg4"=>$responseType)));
        return $ret;
    }
    
    public function cvcRequest(cvcRequest $parameters) {
        $ret = $this->soapClient->__soapCall("cvcRequest", array());
        return $ret;
    }
    
    public function caRenewCertRequest(caRenewCertRequest $parameters) {
        $ret = $this->soapClient->__soapCall("caRenewCertRequest", array());
        return $ret;
    }
    
    public function caCertResponse(caCertResponse $parameters) {
        $ret = $this->soapClient->__soapCall("caCertResponse", array());
        return $ret;
    }
    
    public function pkcs10Request(
            $username, 
            $password, 
            $pkcs10, 
            $hardTokenSN, 
            $responseType) {
        $ret = $this->soapClient->__soapCall("pkcs10Request", array(array(
            "arg0"=>$username, 
            "arg1"=>$password, 
            "arg2"=>$pkcs10, 
            "arg3"=>$hardTokenSN, 
            "arg4"=>$responseType)));
        return $ret;
    }
    
    public function pkcs12Request(
            $username, 
            $password, 
            $hardTokenSN, 
            $keyspec,
            $keyalg) {
        $ret = $this->soapClient->__soapCall("pkcs12Req", array(array(
            "arg0"=>$username, 
            "arg1"=>$password, 
            "arg2"=>$hardTokenSN, 
            "arg3"=>$keyspec, 
            "arg4"=>$keyalg)));
        return $ret->return->keystoreData;
    }
    
    public function keyRecoverNewest(keyRecoverNewest $parameters) {
        $ret = $this->soapClient->__soapCall("keyRecoverNewest", array());
        return $ret;
    }
    
    public function revokeToken(revokeToken $parameters) {
        $ret = $this->soapClient->__soapCall("revokeToken", array());
        return $ret;
    }
    
    public function checkRevokationStatus($issuerDN, $certificateSN) {
        $ret = $this->soapClient->__soapCall("checkRevokationStatus", array(array(
            "arg0"=>$issuerDN, 
            "arg1"=>$certificateSN, 
        )));
        return $ret;
    }
    
    public function fetchUserData(fetchUserData $parameters) {
        $ret = $this->soapClient->__soapCall("fetchUserData", array());
        return $ret;
    }
    
    public function genTokenCertificates(genTokenCertificates $parameters) {
        $ret = $this->soapClient->__soapCall("genTokenCertificates", array());
        return $ret;
    }
    
    public function getHardTokenData(getHardTokenData $parameters) {
        $ret = $this->soapClient->__soapCall("getHardTokenData", array());
        return $ret;
    }
    
    public function getHardTokenDatas(getHardTokenDatas $parameters) {
        $ret = $this->soapClient->__soapCall("getHardTokenDatas", array());
        return $ret;
    }
    
    public function republishCertificate(republishCertificate $parameters) {
        $ret = $this->soapClient->__soapCall("republishCertificate", array());
        return $ret;
    }
    
    public function customLog(customLog $parameters) {
        $ret = $this->soapClient->__soapCall("customLog", array());
        return $ret;
    }
    
    public function deleteUserDataFromSource(deleteUserDataFromSource $parameters) {
        $ret = $this->soapClient->__soapCall("deleteUserDataFromSource", array());
        return $ret;
    }
    
    public function getAuthorizedEndEntityProfiles(getAuthorizedEndEntityProfiles $parameters) {
        $ret = $this->soapClient->__soapCall("getAuthorizedEndEntityProfiles", array());
        return $ret;
    }
    
    public function getAvailableCertificateProfiles($entityProfileId) {
        $ret = $this->soapClient->__soapCall("getAvailableCertificateProfiles", array(array("arg0"=>$entityProfileId)));
        return $ret;
    }
    
    public function getAvailableCAsInProfile($entityProfileId) {
        $ret = $this->soapClient->__soapCall("getAvailableCAsInProfile", array(array("arg0"=>$entityProfileId)));
        return $ret;
    }
    
    public function getPublisherQueueLength(getPublisherQueueLength $parameters) {
        $ret = $this->soapClient->__soapCall("getPublisherQueueLength", array());
        return $ret;
    }
    
    public function certificateRequest(certificateRequest $parameters) {
        $ret = $this->soapClient->__soapCall("certificateRequest", array());
        return $ret;
    }
    
    
    public function softTokenRequest(softTokenRequest $parameters) {
        $ret = $this->soapClient->__soapCall("softTokenRequest", array());
        return $ret;
    }

    
}



?>
